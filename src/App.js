import React, {useEffect, useState} from 'react';
import './App.css';

const App = () => {
    const [data, setData] = useState(null)
    const [editIndex, setEditIndex] = useState(null)
    let headers = [];
    let list = {};

    useEffect(() => {
        getAllFields()
    }, [])

    const getAllFields = async () => {
        var requestOptions = {
            method: 'GET',
        };

        let response = await fetch("http://178.128.196.163:3000/api/records", requestOptions)
        if (response.ok) {
            let json = await response.json();
            json ? setData(json) : setData(null)
        } else {
            alert("Ошибка HTTP: " + response.status);
        }
    }

    const deleteField = async (id) => {
        var requestOptions = {
            method: 'DELETE',
        };

        let response = await fetch("http://178.128.196.163:3000/api/records/" + id, requestOptions)
        if (response.ok) {
            getAllFields()
        } else {
            alert("Ошибка HTTP: " + response.status);
        }
    }

    const addFields = async (item) => {
        let body = JSON.stringify(
            {"data": item}
        )

        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        let requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: body,
            redirect: 'follow'
        };

        let response = await fetch("http://178.128.196.163:3000/api/records", requestOptions)
        if (response.ok) {
            await getAllFields()
        } else {
            alert("Ошибка HTTP: " + response.status);
        }
    }

    const editField = async (id, item) => {
        let body = JSON.stringify(
            {"data": item}
        )

        console.log(body)
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: body,
            redirect: 'follow'
        };

        let response = await fetch("http://178.128.196.163:3000/api/records/" + id, requestOptions)
        if (response.ok) {
            getAllFields()
        } else {
            alert("Ошибка HTTP: " + response.status);
        }
    }


    const renderTableHeader = (data) => {
        data.map((item) => {
            for (let i in item.data) {
                if (!headers.includes(i)) {
                    headers.push(i)
                }
            }
        })

        return (
            <tr>
                {headers.map((item) => {
                    return (
                        <td>{item}</td>
                    )
                })}
            </tr>
        )
    }

    const renderTableBody = (item, itemIndex) => {
        let tableRow = []
        let body = {}
        let id = item._id
        headers.map((h, index) => {
            if (item.data) {
                if (item.data[h]) {
                    tableRow[index] = item.data[h]
                } else {
                    tableRow[index] = ""
                }
            }
        })

        return (
            <tr>
                {tableRow.map((i, ind) => {
                    return (
                        <>
                            {editIndex !== itemIndex ? <td>{i}</td>
                                : <td><input onChange={(e) => {
                                    body[headers[ind]] = e.target.value
                                }} readOnly={editIndex !== itemIndex}/></td>
                            }
                        </>
                    )
                })}

                {item.data ?
                    <><td className='Button' onClick={() => deleteField(id)}>🗑</td>
                    {editIndex === itemIndex ?
                        <td className='Button' onClick={() => {
                            console.log(body)
                            editField(id, body)
                            setEditIndex(null)
                        }}>✔️</td> :
                        <td className='Button' onClick={() => {
                            setEditIndex(itemIndex)
                        }}>✏️</td>}</>: <></>}
            </tr>
        )
    }

    const renderAddFields = (data) => {
        headers.map((h) => {
            list[h] = ""
        })

        return (
            <tr>
                {data.map((item, index) => {
                    return (
                        <td>
                            <input onChange={(e) => {
                                list[headers[index]] = e.target.value
                            }}/>
                        </td>
                    )
                })}

                <td onClick={() => {addFields(list)}}>➕</td>
            </tr>
        )
    }

    return (
        <div className="App">
            {data ?
                <table className="TableData">
                    {renderTableHeader(data)}
                    {data.map(renderTableBody)}
                    {renderAddFields(headers)}
                </table> : <div></div>}
        </div>
    );
}

export default App;
